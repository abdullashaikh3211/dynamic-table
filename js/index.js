const list = document.querySelector('.list');
const btn = document.querySelector('.btn');
const table_obj = new table(2);
table_obj.built();
list.addEventListener('change', function () {
    table_obj.update(this.value);
});
btn.addEventListener('click', function () {
    table_obj.calculate();
});
//# sourceMappingURL=index.js.map