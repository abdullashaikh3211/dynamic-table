const tab= document.querySelector('.table');
class table{
    rows:number;
    constructor(r:number){
        this.rows=r;
    }
    built(){
        tab.innerHTML="<tr><th>Number</th><th>Result</th></tr>";
        for(let i=0;i<this.rows;i++){
            tab.innerHTML+="<tr class='row'><td><input type='number'></td><td>0</td></tr>";
        }
    }
    update(r:number){
        this.rows=r;
        this.built();
    }
    check(): number{
        var row = document.querySelectorAll('.row');
        row.forEach(r=>{
            // console.log(r.childNodes[0].childNodes[0]['value'])
            let v = r.childNodes[0].childNodes[0]['value'];
            if(v==""){
                alert('Please enter all the values or select lower number of rows!');
                return 1;
            }
        })
        return 0
    }
    calculate(){
        if(this.check()){
            return;
        }
        var row = document.querySelectorAll('.row');
        row.forEach(r=>{
                // console.log(r.childNodes[0].childNodes[0]['value'])
                let v = r.childNodes[0].childNodes[0]['value'];
                
                r.childNodes[1]['innerHTML'] = v*v*v;
        })
    
    }
}
